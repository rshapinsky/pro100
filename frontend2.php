<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div id="content"></div>
	<script> 
	for (var i = 1; i <= 100; i++) {
		var res = '';

		if (i % 3 == 0) {
			res += 'foo';
		}

		if (i % 5 == 0) {
			res += 'bar';
		}

		if (res == '') {
			res = i;
		}

		document.getElementById('content').innerHTML += res + "<br>";
	}
	</script> 
</body>
</html>