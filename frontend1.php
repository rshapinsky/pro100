<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			margin: 0;
			padding: 0;
		}

		input{
			box-sizing: content-box;
			width: 390px;
			display: block;
			padding: 18px;
			margin-bottom: 10px;
			border: 0;
			border-radius: 5px;
			font-size: 26px;
			outline:none;
		}

		.email{
			content: ' '; 
			position: absolute; 
			width: 0; 
			height: 0; 
			left: 40px; 
			top: -24px;
			border: 12px solid; 
			border-color: transparent transparent #fff transparent; 
			transition: left 0.5s;
		}

		input::placeholder{
			color: #e0e0e0;
		}

		input[type="text"] {
			color: #797373;
		}

		input[type="submit"] {
			/*font-family: "Crique Grotesk Display";*/
			background: #089bcf;
			color: #fff;
		}

		input[type="submit"]:hover{
			background: #09769c;
		}

		form{
			position: relative;
		}

		.container {
			background: #6ed0f7;
			display: table;
			margin: auto;
			margin-top: 15%;
			padding-top: 20px;
			padding-left: 180px;
			padding-right: 180px;
			padding-bottom: 20px;
		}

		#dynamic-block{
			overflow: hidden;
		}

		.menu {
			width: 100%;
			display: flex;
			justify-content: flex-start;
			box-sizing: border-box;
		}

		a {
			font-family: "Breul Grotesk A Black",sans-serif;
			font-weight: 900;
			padding-right: 20px;
			padding-bottom: 40px;
			text-decoration: none;
			color: #b3e9fa;
			font-size: 24px;
			cursor: pointer;
		}

		a:hover{
			color: #fff;
		}

		a:nth-child(3){
			padding-right: 0;
			justify-content: flex-end;
			margin-left: auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<div id="content">
			<div class="menu">
				<a onclick="switch_points(0);">SIGN IN</a>
				<a onclick="switch_points(1);">SIGN UP</a>
				<a onclick="switch_points(2);">RESET</a>
			</div>
			<form onsubmit="return false;">
				<div class="email"></div>
				<input id="email" type="text" name="email" placeholder="Email">
				<div id="dynamic-block">
					<input id="password" type="text" name="password" placeholder="Password">
					<input id="repeat_password" type="text" name="repeat_password" placeholder="Repeat password">
				</div>
				<input id="submit" type="submit" value="Sign in">
			</form>
		<div>
	</div>
	<script> 
	var stock = {
		height: ''
	};

	var content = document.getElementById('content');
	content.style.height = content.offsetHeight + "px";

	var dynamic_block = document.getElementById('dynamic-block');
	dynamic_block.style.height = dynamic_block.offsetHeight + "px";

	stock.height = dynamic_block.offsetHeight;


	function switch_points(index){

		for (var i = 0; i < 3;i++) {
			if (i == index) {
				document.getElementsByTagName("a")[i].style.color = '#fff';
			} else {
				document.getElementsByTagName("a")[i].style.color = '#b3e9fa';
			}
		}

		var email = document.getElementsByClassName('email');
		var dynamic_block = document.getElementById('dynamic-block');

		switch (index) {
			case 0:
				email[0].style.left = '40px';
				dynamic_block.style.height = (stock.height / 2) + "px";
			    break;
			case 1:
				email[0].style.left = '160px';
				dynamic_block.style.height = stock.height + "px";
			    break;
			case 2:
				email[0].style.left = '370px';
				dynamic_block.style.height = "0px";
			    break;
		}
	}

	switch_points(0);
	setTimeout(function() { 
		dynamic_block.style.transition = "height 0.5s";
	},100);
	</script> 
</body>
</html>
