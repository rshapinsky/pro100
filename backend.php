<?php 
	header ("Content-Type: text/html; charset=utf-8");

	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$sum_test_cases = [
	    ['1', '2', '3'],
	    ['1', '100', '101'],
	    ['0', '0', '0'],
	    ['1234567', '1', '1234568'],
	    ['9999999', '1', '10000000'],
	    // Следующие значения значительно больше, чем Int64
	    ['9923372036854775807', '9923372036854775807', '19846744073709551614'],
	    ['8547758079923372036', '282076016637471277188', '290623774717394649224'],
	    ['716677228283328742446576', '88239450377928284756249776848', '88240167055156568084992223424'],
	];

	//echo add('99', '1');

	function add(string $left, string $right)//: string
	{
		$array = array($left, $right);
		$array_rev = array();
		
		$length = 0;
		foreach ($array as $k => $v) {
			$array_rev[$k] = '';

			$l = strlen($v) - 1;

			for ($i = $l; $i >= 0 ; $i--) { 
				$array_rev[$k] .= $v[$i];

				if ($l > $length) {
					$length = $l;
				}
			}
		}

		$result = '';

		$h = 0;
		for ($i = 0; $i <= $length; $i++) { 
			$ch1 = !empty($array_rev[0][$i]) ? $array_rev[0][$i] : 0;
			$ch2 = !empty($array_rev[1][$i]) ? $array_rev[1][$i] : 0;

			$ch_sum = $ch1 + $ch2 + $h;

			$h = 0;
			if ($ch_sum > 9) {
				$h = floor($ch_sum / 10);
				$ch_sum = $ch_sum % 10;
			}
			$result = $ch_sum.$result;
		}

		if ($h != 0) {
			$result = $h.$result;
		}

		return $result;
	}

	foreach ($sum_test_cases as $case) {
	    list($left, $right, $expected_result) = $case;
	    assert(add($left, $right) === $expected_result);
	}
?>